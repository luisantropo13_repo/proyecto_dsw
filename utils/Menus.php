<?php
    CONST ARRAY_MENU = 
    [
        ["texto"=>"Inicio","url"=>"index.php"],
        ["texto"=>"Galleria","url"=>"gallery.php"],
        ["texto"=>"Sobre Nosotros","url"=>"about.php"],
        ["texto"=>"Contacto","url"=>"contact.php"],
        ["texto"=>"Upload Images","url"=>"upload.php","require_login"=> Menus::LOGIN_USER]
    ];


    class Menus{
        CONST ALL_USER = 0;
        CONST LOGIN_USER = 1;
        CONST NO_LOGIN_USER = 2;

        private static function isPATH ( $data ) 
        {
            $p = explode("." ,  (basename($_SERVER['PHP_SELF']) ) ) ;

            return ( $data == basename($_SERVER['PHP_SELF']) || $data == $p[0] );

        }

        static function create_menu($data){
            echo "<ul>";
               for($i = 0; $i<count($data); $i++)
               {    $next = true;

                    $data[$i]["require_login"] = $data[$i]["require_login"] ?? Menus::ALL_USER;

                    switch ($data[$i]["require_login"]) {
                        case Menus::LOGIN_USER:
                            if(!isset($_SESSION["user"])) $next = false;
                        break;

                        case Menus::NO_LOGIN_USER:
                            if(isset($_SESSION["user"])) $next = false;
                        break;
                        
                    }
                   
                    if( !$next ) continue;


                    $esto_url=$data[$i]["url"];
                    $esto_texto=$data[$i]["texto"];
                    $esto_selected=( self::isPATH($esto_url) ) ? "class='colorlib-active'":"";

                    echo"<li $esto_selected><a href='$esto_url'>$esto_texto</a></li>";
               }

            echo "</ul>";
        }
    }

 
?>