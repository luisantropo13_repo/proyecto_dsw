<aside id="colorlib-aside" role="complementary" class="js-fullheight">

			<h1 id="colorlib-logo" class="mb-4 mb-md-5">
				<a href="index.php" style="background-image: url(images/mf.gif); font-size:150px;">Luis</a>
			</h1>
			<nav id="colorlib-main-menu" role="navigation">
				<?php Menus::create_menu(ARRAY_MENU); ?>
			</nav>

			<div class="colorlib-footer">
				<div class="mb-4">
					<h3>Subscribe for newsletter</h3>
					<form action="#" class="colorlib-subscribe-form">
						<div class="form-group d-flex">
							<div class="icon"><span class="icon-paper-plane"></span></div>
							<input type="text" class="form-control" placeholder="Enter Email Address">
						</div>
					</form>
				</div>
				<p class="pfooter"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib.com</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				</div>
			</aside> <!-- END COLORLIB-ASIDE -->